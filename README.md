# Fco. J. Godoy personal web

## Technologies

- [**Inuitcss**](https://github.com/inuitcss/inuitcss) - Extensible, scalable, Sass-based, OOCSS framework for large and long-lasting UI projects.
- [**Gulp**](http://gulpjs.com) - Automate and enhance your workflow
- [**Pug**](https://pugjs.org) - Terse language for writing HTML templates.
- [**SASS**](http://sass-lang.com) - CSS with superpowers.
- [**Babel**](https://babeljs.io) - Use next generation JavaScript, today (ES5 => ES6).
- [**InuitCss**](http://inuitcss.com) - Extensible, scalable, Sass-based, OOCSS framework.
- [**NodeJS**](https://nodejs.org) - JavaScript runtime built on Chrome's V8 JavaScript engine.

## Requirements and Use

### Requirements

- [Node.js](https://nodejs.org/en/)
- [Gulp](http://gulpjs.com)

```bash
$ npm install -g gulp
```

### Use

```bash
$ git clone https://github.com/fcojgodoy/franciscogodoy.es.git
$ cd franciscogodoy.es/ && npm install
$ gulp
```

## Tasks

```gulp```: Runs the **default task** (dev) including the following ones :

- ```styles```: SCSS compiling to CSS, css minification and autoprefixing.
- ```templates```: Pug compiling and rendering to HTML.
- ```scripts```: ES6 to ES5 with babel, scripts minification and concatenation into a single file.
- ```images```: Image compression.
- ```beautify```: Beautify your preproduction files at ```./dist/```.
- ```serve```: Starts a server at ```./dist/``` with all your compiled files, looking for file changes and injecting them into your browser.

```gulp build```: Builds your project. runs the following tasks:

- ```styles```
- ```templates```
- ```scripts```
- ```images```
- ```beautify```

```gulp optimize```: Optimizes your project (to improve the pagespeed) runs:

- ```uncss```: Removes unused CSS from your styles file using [uncss](https://github.com/giakki/uncss).
- ```critical```: Extract and inline critical-path (above-the-fold) CSS from HTML using [critical](https://github.com/addyosmani/critical)
- ```images```

```gulp deploy```: Deploy your ```dist``` folder into your server or surge cloud runs:

- ```optimize```
- ```ftp```: Uploads ```dist``` to [```ftpUploadsDir```](./gulpfile.babel.js).
- ```surge```: Uploads your ```dist``` to [Surge](http://surge.sh)

If you want to use the **deploy** task, you will have to edit the ftpCredentials in [```gulpfile.babel.js```](./gulpfile.babel.js). If you want to use [Surge](http://surge.sh) instead of FTP, just setup a domain name in the [```surgeInfo.domain```](./gulpfile.babel.js)

Once you setup ```ftpCredentials```, you will have to choose a directory of your server where the deploy will go: [```ftpUploadsDir```](./gulpfile.babel.js)

Now you will be able to use ```gulp deploy``` and your ```/dist/``` folder will go up to your ftp server!

Use ```npm run``` to list the gulp tasks available. You can run the tasks too using the ```npm run scriptname``` that is on the list.


## Project Structure

```
.
├── /dist/                   # Minified, optimized and compiled files.
│   ├── /assets/             # Assets folder.
│   │   ├── /css/            # CSS style files.
│   │   ├── /files/          # Static files.
│   │   │   └── img/         # Images folder.
│   │   └── /js/             # JS files.
│   └── *.html               # Rendered and compiled HTMLs from Pug.
├── /node_modules/           # Node modules dependencies and packages.
├── /src/                    # Source files.
│   ├── /images/             # Images non compressed.
│   ├── /scripts/            # JavaScript files.
│   ├── /styles/             # SCSS style files.
│   ├── /templates/          # Templating Pug files.
│   │   └── _includes/       # Templating Pug partials.
└── 
```
