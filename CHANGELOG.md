# Changelog

## [2.1.0] - 2018-10-19
### New features
- Add new components
### New content
- Add 'Services' section.
- Add 'Corporative badget' page.

## [2.0.8] - 2018-09-20
### New features
- Add sitemap create dev functions
### New content
- Add sitemap xml file
### Fix
- Fix duplicate url: /index.html and /
- Remove unused npm package

## [2.0.7] - 2018-09-19
### Fix
- Fix navbar fixed spacing and main element margin top
- Fix changelog data error

## [2.0.6] - 2018-09-18
### New content
- Add link to this proyect in GitLab
- Add image to projects cards
- New nav-bar object
- Change projects cards order


## [2.0.5] - 2018-09-17
### Breaking changes
### New features
- Add GTM codes
### New content
- Improve introduction text
- Add iConecta project
- Add RACC project
### Fixes
- Minor styles fixes
- Remove 'index.html' from nav
### Minor changes
- Minor dev changes
- Minor Readme changes


## [2.0.4] - 2018-07-31
### New content
- Add Teruterubozu WordPress.org links


## [2.0.3] - 2018-07-25
### New content
- Add WP Add User Contact Fields article
- Add Teruterubozu article
- Add style for disabled buttons
- Improve version changelog template


## [2.0.1] - 2018-05-18
### Fixes
- Fix copy errors.
- Create CHANGELOG.md
- Optimize styles


## [2.0.0] - 2018-05-18
### Breaking changes
- Massive refactoration with Starterkit framework.

<!-- ## [2.0.0] - 2018-05-18

### Breaking changes

### New features

### New content

### Fixes

### Minor changes -->
